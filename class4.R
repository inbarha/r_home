# bike examples 

bike <- read.csv('train.csv')
str(bike)

#����� ���� ������� �� ���� datrtime
# "[", 1 - ��� ������ �� ����� 

bike$datetime <- as.character(bike$datetime)
bike$date <- sapply(strsplit(bike$datetime, ' '), "[", 1)

bike$date <- as.Date(bike$date)

bike$time <- sapply(strsplit(bike$datetime, ' '), "[", 2)

bike$hour <- sapply(strsplit(bike$time, ':'), "[", 1)

bike$hour <- as.numeric(bike$hour)

#����� �����
bike$time <- NULL

bike$season <- factor(bike$season ,levels = c(1,2,3,4), labels = c('spring','aummer','fall','winter'))

library(ggplot2)

#how tempertaure affect count 
pl <- ggplot(bike, aes(temp,count))+ geom_point(alpha = 0.3 ,aes(color =temp))

#how date affect count
p11<- ggplot(bike , aes(date,count)) + geom_point(alpha = 0.5, aes(color =temp))
p11+ scale_color_continuous(low = 'blue', high = 'red')

#how season affect count
pl2 <- ggplot(bike, aes(season, count)) +geom_boxplot(aes(color= season))

#how hour affects count on weekdays

pl3 <- ggplot(bike[bike$workingday != 1,], aes(hour,count))
pl3 <- pl3 + geom_point(position = position_jitter(w = 0.5, h = 0), aes(color=temp))

#how hour affects count on weekdays

pl4 <- ggplot(bike[bike$workingday == 1,], aes(hour,count))
pl4 <- pl4 + geom_point(position = position_jitter(w = 0.5, h = 0), aes(color=temp))

bike$datetime <- NULL


splitDate <- as.Date('2012-05-01')

dateFilter <- bike$date <= splitDate

bike.train <- bike[dateFilter,]
bike.test <-  bike[!dateFilter,]

model <-lm(count ~ . -atemp -casual -registered -date,bike.train )

summary(model)

predict.train <- predict(model,bike.train)
predict.test <- predict(model,bike.test)


MSE.train <- mean((bike.train$count-predict.train)**2)
MSE.test <- mean((bike.test$count-predict.test)**2)

RMSE.train <- MSE.train**0.5
RMSE.test <- MSE.test**0.5


